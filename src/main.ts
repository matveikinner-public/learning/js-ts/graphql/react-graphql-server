import { NestFactory } from "@nestjs/core";
import { ConfigService } from "@nestjs/config";
import { ConfigEnum, SharedConfigOptions } from "@configs/index";
import { AppModule } from "./app.module";

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(ConfigService);

  const sharedConfig = configService.get<SharedConfigOptions>(ConfigEnum.SHARED);

  app.setGlobalPrefix("api");

  app.enableCors();

  await app.listen(sharedConfig.port || 3000);
};

void bootstrap();
