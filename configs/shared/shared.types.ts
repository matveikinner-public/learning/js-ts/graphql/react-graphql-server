interface SharedConfigOptions {
  port: number;
  name: string;
  version: string;
}

export default SharedConfigOptions;
