import { registerAs } from "@nestjs/config";
import { ConfigEnum } from "..";
import SharedConfigOptions from "./shared.types";

const SERVER_NAME = "React GraphQL Server";

export default registerAs(
  ConfigEnum.SHARED,
  (): SharedConfigOptions => ({
    port: parseInt(process.env.PORT) || 3000,
    name: process.env.NODE_ENV ? `${SERVER_NAME} - ${process.env.NODE_ENV.toUpperCase()}` : SERVER_NAME,
    version: process.env.VERSION || "0.0.1",
  })
);
