export enum ConfigEnum {
  SHARED = "SHARED",
}

export type { default as SharedConfigOptions } from "./shared/shared.types";
export { default as sharedConfig } from "./shared/shared.config";
